{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-credentials>` Description.

    Example:

    ```html
    <cells-bfm-credentials></cells-bfm-credentials>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-credentials | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmCredentials extends Polymer.Element {

    static get is() {
      return 'cells-bfm-credentials';
    }

    static get properties() {
      return {
        title: Object,
        form: Object,
        inputAccount: Object,
        inputPasword: Object,
        hasError: {
          type: Boolean,
          Value: false
        }
      };
    }

    _sendInfo() {
      this.dispatchEvent(new CustomEvent('send-data', {detail: this.form.buttonEvent}));
    }
  }

  customElements.define(CellsBfmCredentials.is, CellsBfmCredentials);
}